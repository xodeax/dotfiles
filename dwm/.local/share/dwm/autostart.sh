#!/bin/bash
wmname LG3D &
#nitrogen --restore &
sleep 3 && ~/.fehbg &
dwmblocks &
compton &
sleep 5 && firefox &
#sleep 5 && thunderbird &
vdirsyncer sync &
/home/odea/.local/bin/getfbevents &
sleep 5 && conky &
redshift &
